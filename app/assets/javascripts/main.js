$(document).ready(function(){
	$(".mail-btn").bind('click', function(){
		openform('mail-form', 285, 285);
	});

    $(".close-form").bind('click', function() {
        $(this).parent().hide();
        $(".overlay").hide();
    });

    $(".vacancy-btn").bind('click', function(){
        openform('vacancy-form', 130, 285);
    });
});

function ch_slide() {
	//$(".bg-mask").animate({opacity: 0.5}, 500, function(){
			$(".top-image").css('background-image', 'url('+slider[current_slide][1]+')').animate({backgroundPositionX: "0px"}, 1, function(){});
			$(".slider-marks li").css('color', 'white');
			$(".slider-marks li[data-id="+slider[current_slide][0]+"]").css('color', slider[current_slide][2]);
			$(".subheader").css('background', slider[current_slide][2]);
			$(".header").empty().append(slider[current_slide][3]);
			$(".slider-content").empty().append(slider[current_slide][4]);
			current_slide++;
			if(current_slide == slider.length)
				current_slide = 0;
			//$(".bg-mask").animate({opacity: 0}, 500, function(){});
	//});
}

function  getPageSize(){
    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else if (document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){ // Explorer 6 strict mode
        xScroll = document.documentElement.scrollWidth;
        yScroll = document.documentElement.scrollHeight;
    } else { // Explorer Mac...would also work in Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    if (self.innerHeight) { // all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if(yScroll < windowHeight){
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }

    // for small pages with total width less then width of the viewport
    if(xScroll < windowWidth){
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }

    return [pageWidth,pageHeight,windowWidth,windowHeight];
}

function openform(form_name, form_top, form_left)
{
    $("."+form_name).css('top', getPageSize()[3]/2-form_top);
    $("."+form_name).css('left', getPageSize()[2]/2-form_left);
    $("."+form_name).css('display', 'block');
    $(".overlay").show();
}