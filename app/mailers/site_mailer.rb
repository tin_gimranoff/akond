#coding: utf-8
class SiteMailer < ActionMailer::Base
  default from: "robot@acond.ru"

  def getcall_email(name, topic, phone, email, message)
  	@name = name
  	@topic = topic
  	@phone = phone
  	@email = email
  	@message = message
  	mail(to: 'gimranov.valentin@yandex.ru', subject: 'Заяввка на обратный звонок') do |format|
      format.text { render "getcall_email" }
    end
  end

  def vacancy_email(name, phone)
  	@name = name
  	@phone = phone
  	mail(to: 'gimranov.valentin@yandex.ru', subject: 'Отклик на вакансию') do |format|
      format.text { render "vacancy_email" }
    end
  end
end
