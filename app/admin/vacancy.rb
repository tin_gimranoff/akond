#coding: utf-8
ActiveAdmin.register Vacancy do

  menu label: "Вакансии"
  permit_params :name, :ob, :tr, :price, :image
  index do
    selectable_column
    id_column
    column :name
    column :price
    column :created_at
    column :updated_at
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая новость" do
        f.input :name
        f.input :ob, :as => :ckeditor
        f.input :tr, :as => :ckeditor
        f.input :price
        f.input :image, :required => false, :as => :file
      end
      f.actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
