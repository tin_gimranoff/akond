#coding: utf-8
ActiveAdmin.register Company do


  menu name: "Компании"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :description, :name, :logo, :url
  
  index do
    selectable_column
    id_column
    column :name
    column :description
    column :url
    column :created_at
    column :updated_at
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новый компания" do
        f.input :name
        f.input :description
        f.input :logo, :required => false, :as => :file
        f.input :url
      end
      f.actions
  end


end
