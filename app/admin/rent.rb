#coding: utf-8
ActiveAdmin.register Rent do

  menu label: "Предложения по аренде"
  permit_params :name, :city, :address, :sq, :price, :image, :intro, :company_id
  index do
    selectable_column
    id_column
    column :name
    column :city
    column :address
    column :sq
    column :price
    column :created_at
    column :updated_at
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новое предложение по аренде" do
        f.input :name
        f.input :city
        f.input :address
        f.input :sq
        f.input :price
        f.input :image, as: :file, require: false
        f.input :intro, :as => :ckeditor
        f.input :company_id, as: :select, collection: Company.all
      end
      f.actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
