#coding: utf-8
ActiveAdmin.register News do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  menu label: "Новости"
  permit_params :name, :introtext, :alias, :content
  index do
    selectable_column
    id_column
    column :name
    column :introtext
    column :alias
    column :created_at
    column :updated_at
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая новость" do
        f.input :name
        f.input :introtext
        f.input :alias
        f.input :content,:as => :ckeditor
      end
      f.actions
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
