#coding: utf-8
ActiveAdmin.register Textpage do

  menu label: "Текстовые страницы"
  permit_params :name, :content, :alias
  index do
    selectable_column
    id_column
    column :name
    column :alias
    column :created_at
    column :updated_at
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая категория" do
        f.input :name
        f.input :alias
        f.input :content, :as => :ckeditor
      end
      f.actions
  end

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
