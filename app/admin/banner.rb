#coding: utf-8
ActiveAdmin.register Banner do

  menu name: "Баннеры"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :description, :label, :image, :url
  
  index do
    selectable_column
    id_column
    column :label
    column :description
    column :url
    column :created_at
    column :updated_at
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новый баннер" do
        f.input :label
        f.input :description
        f.input :image, :required => false, :as => :file
        f.input :url
      end
      f.actions
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
