#coding: utf-8
ActiveAdmin.register Article do

  menu label: "Статьи"
  permit_params :name, :introtext, :image, :content, :source
  index do
    selectable_column
    id_column
    column :name
    column :source
    column :introtext
    column :created_at
    column :updated_at
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая статья" do
        f.input :name
        f.input :introtext, :as => :ckeditor
        f.input :content, :as => :ckeditor
        f.input :source
        f.input :image, :required => false, :as => :file
      end
      f.actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
