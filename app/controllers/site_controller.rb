class SiteController < ApplicationController

  before_filter :construct
  
  def index
  	@sliders = Slider.all
  	@news = News.limit(4)
    @rents = Rent.limit(4)
  end

  def companies
  	@news = News.limit(4)
  	@companies = Company.all
  end

  def company
    @news = News.limit(4)
    @company = Company.find(params[:id]) rescue nil
    if !@company
      riderect_to '/404'
      return
    end

    if @company.rent.count == 0
      @pageinfo = @company
      render :textpage
    else
      render :company
    end

  end

  def rent
    @news = News.limit(4)
    @rent = Rent.find(params[:rent_id]) rescue nil
    @company = Company.find(params[:id]) rescue nil
    redirect_to '/404' if @rent.nil? || @company.nil?
    rents_for_company = Rent.where('company_id = ? AND id <> ?', params[:id], params[:rent_id])
    rents = Rent.where('company_id <> ?', params[:id]).limit(4-rents_for_company.count)
    @rents = rents_for_company | rents
  end 

  def work
  	@news = News.limit(4)
  	@vacansies = Vacancy.all
  	if !params[:id]
  		render :work
  	else
  		@vacancy_info = Vacancy.find(params[:id])
  		render :work_inner
  	end
  end

  def article
    @news = News.limit(4)
    if !params[:id]
      @articles = Article.limit(7).order('id DESC')
      render :articles
    else
      @article = Article.find(params[:id])
      @articles = Article.where("id <> "+params[:id]).limit(3)
      render :article
    end
  end

  def news
    @new = News.where(alias: params[:alias])[0]
    @news = News.where("id <> "+@new.id.to_s).limit(4)
  end

  def textpage
    @news = News.limit(4)
    @pageinfo = Textpage.where(alias: params[:slug])[0]
    if !@pageinfo
      render '404'
    else
      render :textpage
    end
  end

  def search
    if params[:q]
      addition_text = AddText.where('content LIKE "%'+params[:q]+'%"');
      render :json => addition_text
    end
  end

  private 
    def construct
        if params[:getcall]
          @getcall = Getcall.new(params[:getcall])
          if @getcall.valid?
             SiteMailer.getcall_email(@getcall.name, @getcall.topic, @getcall.phone, @getcall.email, @getcall.message).deliver
          end
        else
           @getcall = Getcall.new
        end

        if params[:vacancy_form]
          @vacancy = VacancyForm.new(params[:vacancy_form])
          if @vacancy.valid?
            SiteMailer.vacancy_email(@vacancy.name, @vacancy.phone).deliver
          end
        else
          @vacancy = VacancyForm.new
        end
    end
end