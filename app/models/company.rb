class Company < ActiveRecord::Base

	has_many :rent

	has_attached_file :logo, :styles => { :thumb => "305x116>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates :name, :description, presence: true

	validates_attachment_content_type :logo, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
