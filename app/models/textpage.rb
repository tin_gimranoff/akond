class Textpage < ActiveRecord::Base
		validates :name, :alias, :content, presence: true
end
