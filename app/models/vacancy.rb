class Vacancy < ActiveRecord::Base
	has_attached_file :image, :styles => { :thumb => "180x126>", :medium => "314x220>", :small => "100x70>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates :name, :tr, :ob, :price, presence: true

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
