class AddText < ActiveRecord::Base
  has_attached_file :attached, :path => ":rails_root/public/:class/:attachment/:id/:basename.:extension", :url => "/:class/:attachment/:id/:basename.:extension"
  
  validates :label, :description, presence: true
  
  validates_attachment_content_type :attached, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif", "application/pdf", "application/x-pdf"]
end
