class Rent < ActiveRecord::Base
	belongs_to :company

	has_attached_file :image, :styles => { :thumb => "165x100>", :inner => "450x270>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
