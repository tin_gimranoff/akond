class News < ActiveRecord::Base
	validates :name, :introtext, :alias, :content, presence: true
end
