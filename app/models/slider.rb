class Slider < ActiveRecord::Base
    
    has_attached_file :image, :path => ":rails_root/public/:class/:attachment/:id/:basename.:extension", :url => "/:class/:attachment/:id/:basename.:extension"
    
    validates :name, :description, :color, presence: true
    validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
    
end
