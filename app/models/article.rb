class Article < ActiveRecord::Base
	has_attached_file :image, 
		:styles => { :thumb_1 => "314x220>", :thumb_2 => "119x74>", :thumb_3 => "309x131>", :thumb_4 => "59x59>", :thumb_5 => "180x126>", :thumb_6 => "100x70>" }, 
		:path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", 
		:url => "/:class/:attachment/:id/:style_:basename.:extension"
	validates :name, :source, :introtext, :content, presence: true

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
