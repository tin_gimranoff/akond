-- MySQL dump 10.13  Distrib 5.6.21, for osx10.9 (x86_64)
--
-- Host: localhost    Database: akond_development
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_admin_comments`
--

DROP TABLE IF EXISTS `active_admin_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_admin_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `resource_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resource_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `author_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_active_admin_comments_on_namespace` (`namespace`),
  KEY `index_active_admin_comments_on_author_type_and_author_id` (`author_type`,`author_id`),
  KEY `index_active_admin_comments_on_resource_type_and_resource_id` (`resource_type`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_admin_comments`
--

LOCK TABLES `active_admin_comments` WRITE;
/*!40000 ALTER TABLE `active_admin_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_admin_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `add_texts`
--

DROP TABLE IF EXISTS `add_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `add_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `attached_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attached_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attached_file_size` int(11) DEFAULT NULL,
  `attached_updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `add_texts`
--

LOCK TABLES `add_texts` WRITE;
/*!40000 ALTER TABLE `add_texts` DISABLE KEYS */;
INSERT INTO `add_texts` VALUES (1,'text_on_main','Текст на главной','<img src=\"/ckeditor_assets/pictures/1/content_content-img.png\" style=\"float: left;margin-right: 22px;margin-bottom: 20px;\">\r\n			<h3 style=\"\">Microsoft Windows Phone 8.1 установлена на 11,9% WP-смартфонов</h3>\r\n			<span style=\"\">бенчмарка на планшете Onda V989, результат синтетического тестирования данного устройства впечатляет: \r\n			планшету удалось набрать рекордные 55 527 очков. Подлинность видеозаписи не была подтверждена, \r\n			однако стоит отметить, что аппаратное обеспечение новинки вполне способно продемонстрировать \r\n			подобный результат. бенчмарка на планшете Onda V989, результат синтетического тестирования данного устройства впечатляет: \r\n			планшету удалось набрать рекордные 55 527 очков. Подлинность видеозаписи не была подтверждена, \r\n			однако стоит отметить, что аппаратное обеспечение новинки вполне способно продемонстрировать \r\n			подобный результат. бенчмарка на планшете Onda V989, результат синтетического тестирования данного устройства впечатляет: \r\n			планшету удалось набрать рекордные 55 527 очков. Подлинность видеозаписи не была подтверждена, \r\n			однако стоит отметить, что аппаратное обеспечение новинки вполне способно продемонстрировать \r\n			подобный результат. бенчмарка на планшете Onda V989, результат синтетического тестирования данного устройства впечатляет: \r\n			планшету удалось набрать рекордные 55 527 очков. Подлинность видеозаписи не была подтверждена, \r\n			однако стоит отметить, что аппаратное обеспечение новинки вполне способно продемонстрировать \r\n			подобный результат.\r\n			</span>',NULL,NULL,NULL,NULL,'2014-11-06 08:18:12','2014-11-06 08:18:12'),(2,'presentation_on_main','Презентация на главной','','DV_2016_Instructions_Russian.pdf','application/pdf',821036,'2014-11-06 08:22:35','2014-11-06 08:22:35','2014-11-06 08:22:35'),(3,'work_header','Заголовок на странице вакансий','Работа В ГК &quot;АКОНД&quot;\r\n',NULL,NULL,NULL,NULL,'2014-11-06 11:28:35','2014-11-06 11:28:50'),(4,'work_article','Текст на странице вакансий','Несколько недель назад мы писали о том, что восьмиядерный \r\n						планшетный компьютер Onda V989 смог набрать 48 102 балла \r\n						в бенчмарке AnTuTu, однако сегодня появилась информация, \r\n						что предыдущие данные не соответствуют действительности. \r\n						Согласно появившейся в Сети видеозаписи, демонстрирующей \r\n						запущенное приложение однако сегодня появилась информация, \r\n						что предыдущие данные не соответствуют однако сегодня \r\n						появилась информация, что предыдущие данные не соответствуют ',NULL,NULL,NULL,NULL,'2014-11-06 11:30:05','2014-11-06 11:30:05');
/*!40000 ALTER TABLE `add_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admin_users_on_email` (`email`),
  UNIQUE KEY `index_admin_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'gimranov.valentin@yandex.ru','$2a$10$exh9SPi0et15w12Ulpho1eTwKxv.Dn.snGqj003T.Its9SHT2ZlQW',NULL,NULL,NULL,4,'2014-11-06 14:45:19','2014-11-06 07:30:08','192.168.0.108','127.0.0.1','2014-11-05 19:34:17','2014-11-06 14:45:19');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Статья','Несколько недель назад мы писали о том, что восьмиядерный планшетный компьютер Onda V989 смог набрать 48 102 балла в бенчмарке AnTuTu, однако сегодня появилась\r\n','Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях\r\n','http://ya.ru','news-1.png','image/png',126332,'2014-11-06 13:21:13','2014-11-06 13:21:14','2014-11-06 13:48:07'),(2,'Статья','Несколько недель назад мы писали о том, что восьмиядерный планшетный','Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях','http://ya.ru','news-1.png','image/png',126332,'2014-11-06 13:21:59','2014-11-06 13:22:01','2014-11-06 13:22:01'),(3,'Статья','Несколько недель назад мы писали о том, что восьмиядерный планшетный','Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях','http://ya.ru','news-1.png','image/png',126332,'2014-11-06 13:23:29','2014-11-06 13:23:30','2014-11-06 13:23:30'),(4,'Статья','Несколько недель назад мы писали о том, что восьмиядерный планшетный','Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях','http://ya.ru','news-1.png','image/png',126332,'2014-11-06 13:24:15','2014-11-06 13:24:16','2014-11-06 13:24:16'),(5,'Статья','Несколько недель назад мы писали о том, что восьмиядерный планшетный','Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях','http://ya.ru','news-1.png','image/png',126332,'2014-11-06 13:25:02','2014-11-06 13:25:03','2014-11-06 13:25:03'),(6,'Статья','Несколько недель назад мы писали о том, что восьмиядерный планшетный','Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях','http://ya.ru','news-1.png','image/png',126332,'2014-11-06 13:25:32','2014-11-06 13:25:33','2014-11-06 13:25:33'),(7,'Статья','Несколько недель назад мы писали о том, что восьмиядерный планшетный компьютер Onda V989 смог набрать 48 102 балла в бенчмарке AnTuTu, однако сегодня появилась информация, что предыдущие данные не соответствуют действительности. Согласно появившейся в Сети видеозаписи, демонстрирующей запущенное приложение','Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях Из комментариев экспертов, анализирующих законопроект, не всегда можно определить, когда именно предпринимательский риск реорганизован. Предпринимательский риск поручает объект права. Кредитор, как можно доказать с помощью не совсем тривиальных допущений, использует уголовный сервитут. Если в соответствии с законом допускается самозащита права, фирменное наименование защищает страховой полис. В соответствии с общим принципом, установленным Конституцией РФ, регрессное требование законодательно подтверждает гарант, учитывая недостаточную теоретическую проработанность этой отрасли права. В ряде недавних судебных решений рефинансирование подведомственно арбитражному суду. Делькредере виновно индоссирует ничтожный товарный кредит. Право собственности страхует диспозитивный аккредитив. Товарный кредит реорганизован. Перестрахование, вследствие публичности данных отношений, последовательно требует регрессный штраф. Акционерное общество принудительно изъято. Аналогия закона трансформирует дебиторский Указ. Субаренда, в представлениях','http://ya.ru','news-1.png','image/png',126332,'2014-11-06 13:26:06','2014-11-06 13:26:07','2014-11-06 13:26:07');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'right_banner_on_main','Баннер на главной в правой колонке','right-banner.png','image/png',997,'2014-11-06 08:53:20','http://ya.ru','2014-11-06 08:53:20','2014-11-06 09:06:42'),(2,'footer_left','Левый баннер в футере','bottom-banner.png','image/png',771,'2014-11-06 10:33:10','','2014-11-06 10:33:10','2014-11-06 10:38:38'),(3,'footer_center','Центральный баннер в футере','bottom-banner.png','image/png',771,'2014-11-06 10:33:44','','2014-11-06 10:33:44','2014-11-06 10:33:44'),(4,'footer_right','Правый баннер в футере','bottom-banner.png','image/png',771,'2014-11-06 10:34:08','','2014-11-06 10:34:08','2014-11-06 10:34:08'),(5,'work_banner','Баннер на странице вакансий','work_banner.png','image/png',53442,'2014-11-06 11:27:11','','2014-11-06 11:27:11','2014-11-06 11:27:11');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ckeditor_assets`
--

DROP TABLE IF EXISTS `ckeditor_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ckeditor_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `assetable_id` int(11) DEFAULT NULL,
  `assetable_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ckeditor_assetable_type` (`assetable_type`,`type`,`assetable_id`),
  KEY `idx_ckeditor_assetable` (`assetable_type`,`assetable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ckeditor_assets`
--

LOCK TABLES `ckeditor_assets` WRITE;
/*!40000 ALTER TABLE `ckeditor_assets` DISABLE KEYS */;
INSERT INTO `ckeditor_assets` VALUES (1,'content-img.png','image/png',192242,1,'AdminUser','Ckeditor::Picture',392,237,'2014-11-06 08:16:55','2014-11-06 08:16:55');
/*!40000 ALTER TABLE `ckeditor_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `logo_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_file_size` int(11) DEFAULT NULL,
  `logo_updated_at` datetime DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Название компании','Несколько недель назад мы писали о том, что восьмиядерный планшетный компьютер Onda V989 смог набрать 48 102 балла в бенчмарке AnTuTu, однако сегодня появилась информация, что предыдущие данные не соответствуют действительности. Согласно появившейся в Сети видеозаписи, демонстрирующей запущенное приложение','company.png','image/png',56516,'2014-11-06 10:54:25','http://ya.ru','2014-11-06 10:54:26','2014-11-06 10:54:26'),(2,'Название компании','Несколько недель назад мы писали о том, что восьмиядерный планшетный компьютер Onda V989 смог набрать 48 102 балла в бенчмарке AnTuTu, однако сегодня появилась информация, что предыдущие данные не соответствуют действительности. Согласно появившейся в Сети видеозаписи, демонстрирующей запущенное приложение','company.png','image/png',56516,'2014-11-06 10:54:53','','2014-11-06 10:54:53','2014-11-06 10:54:53');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'BQS-4700 Harvard появился в продаже в России','Согласно информации, поступившей от компании BQ, смартфон .','news-1','Lorem ipsum lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum\r\n','2014-11-05 21:10:12','2014-11-05 21:38:38'),(2,'BQS-4700 Harvard появился в продаже в России','Согласно информации, поступившей от компании BQ, смартфон .','news-2','Lorem ipsum lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum','2014-11-05 21:39:17','2014-11-05 21:39:17'),(3,'BQS-4700 Harvard появился в продаже в России','Согласно информации, поступившей от компании BQ, смартфон .','news-3','Lorem ipsum lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum','2014-11-05 21:39:39','2014-11-05 21:39:39'),(4,'BQS-4700 Harvard появился в продаже в России','Согласно информации, поступившей от компании BQ, смартфон .','news-4','Lorem ipsum lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum','2014-11-05 21:39:51','2014-11-05 21:39:51'),(5,'BQS-4700 Harvard появился в продаже в России','Согласно информации, поступившей от компании BQ, смартфон .','news-5','Lorem ipsum lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum&nbsp;lorem ipsum','2014-11-05 21:40:03','2014-11-05 21:40:03');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20141105124809'),('20141105124816'),('20141105150919'),('20141105204913'),('20141106073842'),('20141106110825'),('20141106110826'),('20141106110827'),('20141106113310'),('20141106123342'),('20141106141154');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,'Аконд','Покупка и продажа собственного недвижимого имущества Подготовка к продаже, покупка и продажа собственного недвижимого имущества, Операции с недвижимым имуществом','#0eff22','bg.png','image/png',851163,'2014-11-05 19:53:36','2014-11-05 19:53:36','2014-11-05 19:53:53'),(2,'Аконд','Покупка и продажа собственного недвижимого имущества Подготовка к продаже, покупка и продажа собственного недвижимого имущества, Операции с недвижимым имуществом','#f9ff00','bg1.png','image/png',789875,'2014-11-05 19:54:15','2014-11-05 19:54:15','2014-11-05 19:54:15'),(3,'Аконд','Покупка и продажа собственного недвижимого имущества Подготовка к продаже, покупка и продажа собственного недвижимого имущества, Операции с недвижимым имуществом','#0034ff','bg2.png','image/png',793746,'2014-11-05 19:54:35','2014-11-05 19:54:35','2014-11-05 19:54:35');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `textpages`
--

DROP TABLE IF EXISTS `textpages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `textpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `textpages`
--

LOCK TABLES `textpages` WRITE;
/*!40000 ALTER TABLE `textpages` DISABLE KEYS */;
INSERT INTO `textpages` VALUES (1,'nedv','НЕДВИЖИМОСТЬ','Lorem ipsum lorem ipsum  lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum','2014-11-06 14:20:31','2014-11-06 14:20:31'),(2,'stroy','СТРОЙМАТЕРИАЛЫ','Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum','2014-11-06 14:21:07','2014-11-06 14:21:07'),(3,'metall','МЕТАЛЛОИЗДЕЛИЯ','Lorem ipsum','2014-11-06 14:21:34','2014-11-06 14:21:34'),(4,'usl','УСЛУГИ','Lorem ipsum','2014-11-06 14:21:57','2014-11-06 14:21:57'),(5,'dev','ДЕВЕЛОПМЕНТ','Lorem ipsum','2014-11-06 14:22:15','2014-11-06 14:22:15'),(6,'ravl','РАЗВЛЕЧЕНИЯ','<p>Lorem ipsum</p>\r\n','2014-11-06 14:22:38','2014-11-06 14:47:34');
/*!40000 ALTER TABLE `textpages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancies`
--

DROP TABLE IF EXISTS `vacancies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tr` text COLLATE utf8_unicode_ci,
  `ob` text COLLATE utf8_unicode_ci,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancies`
--

LOCK TABLES `vacancies` WRITE;
/*!40000 ALTER TABLE `vacancies` DISABLE KEYS */;
INSERT INTO `vacancies` VALUES (1,'Вакансия','<ul>\r\n	<li>Коммуникабельность</li>\r\n	<li>Строго без в/п</li>\r\n	<li>Возможность коммандировок</li>\r\n</ul>\r\n','<ul>\r\n	<li>Коммуникабельность</li>\r\n	<li>Строго без в/п</li>\r\n	<li>Возможность коммандировок</li>\r\n</ul>\r\n','20 тыс.','vacancy-inner-image.png','image/png',136253,'2014-11-06 12:14:36','2014-11-06 11:49:37','2014-11-06 12:14:37'),(2,'Вакансия','<ul>\r\n	<li>Коммуникабельность</li>\r\n	<li>Строго без в/п</li>\r\n	<li>Возможность коммандировок</li>\r\n</ul>\r\n','<ul>\r\n	<li>Коммуникабельность</li>\r\n	<li>Строго без в/п</li>\r\n	<li>Возможность коммандировок</li>\r\n</ul>\r\n','20 тыс.','vacancy-inner-image.png','image/png',136253,'2014-11-06 12:14:52','2014-11-06 11:50:00','2014-11-06 12:14:53');
/*!40000 ALTER TABLE `vacancies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-11 11:08:26
