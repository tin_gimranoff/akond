class CreateRents < ActiveRecord::Migration
  def change
    create_table :rents do |t|
      t.string :name
      t.has_attached_file :image
      t.string :city
      t.string :address
      t.string :sq
      t.string :price
      t.text :intro
      t.references :company

      t.timestamps
    end

    add_index :rents, :company_id
  end
end
