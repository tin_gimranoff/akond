class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :label
      t.string :description
      t.has_attached_file :image
      t.string :url

      t.timestamps
    end
  end
end
