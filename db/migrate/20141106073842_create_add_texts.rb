class CreateAddTexts < ActiveRecord::Migration
  def change
    create_table :add_texts do |t|
      t.string :label
      t.string :description
      t.text :content
      t.has_attached_file :attached

      t.timestamps
    end
  end
end
