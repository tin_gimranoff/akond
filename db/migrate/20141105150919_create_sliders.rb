class CreateSliders < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.string :name
      t.text :description
      t.string :color
      t.has_attached_file :image
      
      t.timestamps
    end
  end
end
