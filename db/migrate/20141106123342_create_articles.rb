class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :name
      t.text :introtext
      t.text :content
      t.string :source
      t.has_attached_file :image

      t.timestamps
    end
  end
end
