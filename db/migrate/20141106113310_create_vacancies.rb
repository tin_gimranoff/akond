class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :name
      t.text :tr
      t.text :ob
      t.string :price
      t.has_attached_file :image

      t.timestamps
    end
  end
end
