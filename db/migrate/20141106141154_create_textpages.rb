class CreateTextpages < ActiveRecord::Migration
  def change
    create_table :textpages do |t|
      t.string :alias
      t.string :name
      t.text :content

      t.timestamps
    end
  end
end
